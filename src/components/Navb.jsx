import React, { Component } from "react";
import { Navbar } from "react-bootstrap";

export default class Navb extends Component {
  render() {
    return (
      <>
        <Navbar bg="dark" variant="dark" className="Navbar">
          <Navbar.Brand href="#home" className="Navbar_brand">
            <img
              alt=""
              src="https://www.pngfind.com/pngs/m/292-2924858_user-icon-business-man-flat-png-transparent-png.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{" "}
            Personal Info React Bootstap
          </Navbar.Brand>
        </Navbar>
      </>
    );
  }
}
