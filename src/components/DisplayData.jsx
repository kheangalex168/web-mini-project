import React, { Component, useState } from "react";
import { Table, Button, Container } from "react-bootstrap";
import MyModal from "./Modal";
export default function DisplayData(props) {
  const [modalShow, setModalShow] = React.useState();
  const [view, setView] = useState([]);

  let onHide = () => {
    setModalShow(false);
  };

  let showView = (id) => {
    let tmp = props.person.filter((item, i) => {
      return item.id === id;
    });
    setView(tmp);
  };

  return (
    <div>
      <MyModal show={modalShow} onHide={onHide} view={view} />

      <Container>
        <Table className="table text-center" striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Email</th>
              <th>Job</th>
              <th>Create At</th>
              <th>Update At</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>
            {props.person.map((item, i) => (
              <tr key={i}>
                <td>{item.id}</td>
                <td>{item.username}</td>
                <td>{item.gender}</td>
                <td>{item.email}</td>
                <td style={{ textAlign: "left" }}>
                  <li>{item.job}</li>
                </td>
                <td>{item.createAt}</td>
                <td>{item.updateAt}</td>
                <td>
                  <Button
                    variant="primary"
                    onClick={() => {
                      setModalShow(true);
                      showView(item.id);
                    }}
                  >
                    <i class="far fa-eye"></i> View
                  </Button>{" "}
                  <Button
                    variant="info"
                    onClick={() => props.handleUpdate(item.id)}
                  >
                    <i class="far fa-edit"></i> Update
                  </Button>{" "}
                  <Button
                    variant="danger"
                    onClick={() => props.handleDelete(item.id)}
                  >
                    <i class="far fa-trash-alt"></i> Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
