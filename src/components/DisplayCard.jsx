import React, { useState } from "react";
import {
  Card,
  Button,
  Container,
  DropdownButton,
  Dropdown,
  Col,
  Row,
} from "react-bootstrap";
import MyModal from "./Modal";

export default function DisplayCard(props) {
  return (
    <div>
      {props.person.map((item, i) => (
        <Row>
          <MyCard
            item={item}
            i={i}
            handleDelete={props.handleDelete}
            handleUpdate={props.handleUpdate}
            person={props.person}
          />
        </Row>
      ))}
    </div>
  );
}

function MyCard(props) {
  const [modalShow, setModalShow] = React.useState();
  const [view, setView] = useState([]);

  let onHide = () => {
    setModalShow(false);
  };

  let showView = (id) => {
    let tmp = props.person.filter((item, i) => {
      return item.id === id;
    });
    setView(tmp);
  };
  return (
    <div>
      <MyModal show={modalShow} onHide={onHide} view={view} />
      <Container>
        <Card className="text-center">
          <Card.Header>
            <DropdownButton id="dropdown-basic-button" title="Action">
              <Dropdown.Item
                onClick={() => {
                  setModalShow(true);
                  showView(props.item.id);
                }}
              >
                View
              </Dropdown.Item>
              <Dropdown.Item onClick={() => props.handleUpdate(props.item.id)}>
                Update
              </Dropdown.Item>
              <Dropdown.Item onClick={() => props.handleDelete(props.item.id)}>
                Delete
              </Dropdown.Item>
            </DropdownButton>
          </Card.Header>

          <Card.Body>
            <Card.Title>{props.item.username}</Card.Title>
            <Card.Text>
              <ul>
                <dl>
                  <dt>Job</dt>
                  <li>{props.item.job}</li>
                </dl>
              </ul>
            </Card.Text>
          </Card.Body>
          <Card.Footer className="text-muted"></Card.Footer>
        </Card>
      </Container>
    </div>
  );
}
