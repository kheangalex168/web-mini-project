import React, { Component } from "react";

export default function GenderJob(props) {
  return (
    <div>
      <h4>Gender</h4>
      <input
        checked
        type="radio"
        value="Male"
        name="gender"
        onChange={props.handleChange}
      />{" "}
      Male{" "}
      <input
        type="radio"
        value="Female"
        name="gender"
        onChange={props.handleChange}
      />{" "}
      Female
      <br></br>
      <h4>Job</h4>
      <input
        type="checkbox"
        checked
        value="Student"
        name="job"
        onChange={props.handleChange}
      />{" "}
      Student{" "}
      <input
        type="checkbox"
        value="Teacher"
        name="job"
        onChange={props.handleChange}
      />{" "}
      Teacher{" "}
      <input
        type="checkbox"
        value="Developer"
        name="job"
        onChange={props.handleChange}
      />{" "}
      Developer
    </div>
  );
}
