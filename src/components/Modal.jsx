import React from "react";
import { Button, Modal } from "react-bootstrap";

export default function MyModal(props) {
  return (
    <div>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Information
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {props.view.map((item, i) => (
              <div key={i} style={{ display: "flex" }}>
                <div style={{ flex: 1 }}>
                  <h4>{item.username}</h4>
                  <p>{item.email}</p>
                  <p>{item.gender}</p>
                </div>

                <div style={{ flex: 1 }}>
                  <p>job</p>
                  <li>{item.job}</li>
                </div>
              </div>
            ))}
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={props.onHide}>Close</Button>
          </Modal.Footer>
        </>
      </Modal>
    </div>
  );
}
