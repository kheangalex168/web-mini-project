import React, { Component } from "react";
import { Form, Button, Col } from "react-bootstrap";

export default function PersonalInfor(props) {
  console.log("update:", props.newPerson[0].username);
  return (
    <>
      <h1>Personal Info</h1>
      {/* ******Form login******** */}

      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Username</Form.Label>
          <Form.Control
            id="username"
            type="username"
            placeholder="username"
            name="username"
            onChange={props.handleChange}
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>
              {props.usernameErr}
              {props.result3}
            </p>
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Email</Form.Label>
          <Form.Control
            id="email"
            type="email"
            placeholder="email"
            name="email"
            onChange={props.handleChange}
          />
        </Form.Group>
        <Form.Text className="text-muted">
          <p style={{ color: "red" }}>{props.emailErr}</p>
        </Form.Text>

        <br></br>
        <div className="button_right">
          <Button
            variant="primary"
            type="button"
            disabled={props.validateBtn()}
            onClick={props.OnSubmit}
          >
            Submit
          </Button>
          ​ <Button variant="secondary">Cancel</Button>
          {/* *****Gender******* */}
        </div>
      </Form>
    </>
  );
}
