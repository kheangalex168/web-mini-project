import React, { Component, useState } from 'react';
import PersonalInfor from './components/PersonalInfor';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navb from './components/Navb';
import { Button, Card, Col,Container,Pagination,Row, Image } from 'react-bootstrap';
import './App.css';
import GenderJob from './components/GenderJob';
import DisplayData from './components/DisplayData';
import DispalyCard from './components/DisplayCard';
import { render } from '@testing-library/react';
import uuid from 'react-uuid'
import moment from 'moment';
  class App extends Component {
    constructor(props){
      super(props)
      this.state =  {
        person: [],
        newPerson: [{
        }],
      show: true,
      }       
    }

   OnDisplay = (showing) => {
      this.setState({
          show: showing
      })
  }

   handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    },
    () => {
      console.log(this.state);
      if (e.target.name === "username") {
        let pattern3 = /^[\w\s-]{3,13}$/g;
        let result3 = pattern3.test(this.state.username);
        if (result3) {
          this.setState({
            usernameErr: "",
          });
        } else if (this.state.username === "") {
          this.setState({
            usernameErr: "Username cannot be empty",
          });
        } else {
          this.setState({
            usernameErr:
              " No more than 9 characters, No less than 3 characters",
          });
        }
      }
      if (e.target.name === "email") {
        let pattern = /^\S+@\S+\.[a-z]{3}$/g;
        let result = pattern.test(this.state.email.trim());
        if (result) {
          this.setState({
            emailErr: "",
          });
        } else if (this.state.email === "") {
          this.setState({
            emailErr: "Email cannot be empty!",
          });
        } else {
          this.setState({
            emailErr: "Email is invalid!",
          });
        }
      }
    }
  );
    console.log(this.state);
  }

   OnSubmit = (e) => {   
    e.preventDefault();
    if (this.state.username === "") {
      return;
    }
    const newItem = {
      id: uuid(),
      username: this.state.username,
      email: this.state.email,
      gender: this.state.gender,
      job: this.state.job,
      createAt: moment().startOf('hour').fromNow(),
      updateAt: moment().startOf('hour').fromNow()
    };
    this.setState((state) => ({
      person: state.person.concat(newItem),
      username: "",
      email: "",
      password: "",
      gender: "",
      showImg: true
    }));

    document.getElementById('username').value = ""
    document.getElementById('email').value = ""
    document.getElementById('img').style.display = "none"
  }

  handleDelete = (id) => {
   let tmp = this.state.person.filter(item=> {
      return item.id !== id
    })
    this.setState({
      person: tmp
    })
  }

  handleUpdate = (id,e) => {
    let tmp = this.state.person.filter(item=> {
      return item.id === id
    })
    this.setState({
      newPerson: tmp,
    })
    document.getElementById('username').value = ""
    document.getElementById('email').value = ""
    document.getElementById('img').style.display = "none"
  }

  validateBtn = () => {
    if (
      this.state.usernameErr === "" &&
      this.state.emailErr === "" &&
      this.state.username !== "" &&
      this.state.email !== ""
    ) {
      return false;
    } else {
      return true;
    }
  }    
    render(){
    return (
      <>   
      <Row>
      <Navb/>
    </Row>
     <Container>
        <Row>
          <Col sm={8}>
          <PersonalInfor validateBtn={this.validateBtn} result3={this.result3} usernameErr={this.state.usernameErr} emailErr={this.state.emailErr} handleChange={this.handleChange} OnSubmit={this.OnSubmit} newPerson={this.state.newPerson}/>
          </Col>
          <Col sm={4} className="gendertop">
            <GenderJob handleChange={this.handleChange} newPerson={this.state.newPerson}/>
          </Col>
        </Row>
        <h2>Display Data As:</h2>
        <Button onClick={()=> this.OnDisplay(true)} className="btn" variant="dark">Table</Button>
        <Button onClick={()=> this.OnDisplay(false)} variant="secondary">Card</Button>
          {
            this.state.show
            ? <Row>  <DisplayData person={this.state.person} handleDelete={this.handleDelete} handleUpdate={this.handleUpdate}/></Row>
            : <Row>
            <Col className="col1" sm={3}>
            <DispalyCard person={this.state.person} handleDelete={this.handleDelete} handleUpdate={this.handleUpdate}/>
            </Col>
          </Row>
          }
          <Row>
          <Image src="https://lh3.googleusercontent.com/proxy/wylYpzgRRveKPeSEB6CodutFskZuhWlqY3xEY72mplFucPQx0ng_TXcbTi-67kh85ZwY3f_kQtOZuA_jHi-zfwoc8jmcwe2uotLGI6s" fluid id="img"/>
          </Row>
          <Row>
          <Col></Col>
          <Col><Pagination>
            <Pagination.First />
            <Pagination.Item>{1}</Pagination.Item>
            <Pagination.Item>{2}</Pagination.Item>
            <Pagination.Item>{3}</Pagination.Item>
            <Pagination.Last />
          </Pagination></Col>
          <Col></Col>
        </Row>
     </Container> 
      </>            
    );
  }
}

export default App;